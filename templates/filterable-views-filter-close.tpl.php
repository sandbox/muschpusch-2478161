/**
 * @file
 * This template handles the close ekement of the views exposed filter form.
 *
 * Variables available:
 * - $text: The text of the closing element.
 */
<div class="state-close"><?php echo $text; ?></div>
